package com.gitlab.ktoublanc;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import static java.util.Optional.ofNullable;

public class RestServiceIT {

    @Test
    public void findAll() {
        final String deployPort = ofNullable(System.getProperty("jboss.http.port")).orElse("8080");
        final String url = "http://localhost:" + deployPort + "/poc-wildfly/rest-application/users/";
        final String users = ClientBuilder.newClient()
                .target(url)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        Assert.assertEquals("[{\"id\":1},{\"id\":2}]", users);
    }
}
package com.gitlab.ktoublanc;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest-application")
public class JAXRSConfiguration extends Application {

}

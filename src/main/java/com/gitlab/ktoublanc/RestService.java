package com.gitlab.ktoublanc;

import javax.ejb.Stateless;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;

@Stateless
@Path("users")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class RestService {

    @GET
    public JsonArray findAll() {
        return createArrayBuilder()
                .add(createObjectBuilder().add("id", 1).build())
                .add(createObjectBuilder().add("id", 2).build())
                .build();
    }

    @GET
    @Path("{id}")
    public JsonObject findById(@PathParam("id") Long id) {
        return createObjectBuilder()
                .add("id", 1)
                .build();
    }
}
